<?php require_once "./code.php";?>



<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S1: PHP Basics and Selection Control Structures</title>
</head>
<body>

	<h1>Full Address</h1>

	<p><?= getFullAddress("Philippines", "Quezon City", "Metro Manila", "Timog Avenue", "Caswynn Bldg.", "3F"); ?></p>
	<p><?= getFullAddress("Philippines", "Makati City", "Metro Manila", "Buendia Avenue", "Enzo Bldg.", "3F"); ?></p>

	<h1>Letter-Based Grading</h1>
	<p>87 is equivalent to <?= getLetterGrade(87); ?></p>
	<p>94 is equivalent to <?= getLetterGrade(94); ?></p>
	<p>74 is equivalent to <?= getLetterGrade(74); ?></p>

</body>
</html>